FROM openjdk:11
MAINTAINER christian.mossberg@sensera.se
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
